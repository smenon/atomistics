from pyscal3 import System
import matplotlib.pyplot as plt
import itertools
import numpy as np

def calculate_rdf(job, rmin=0, rmax=10.00, bins=100):
    system = System(job.get_structure(), format='ase')
    system.find.neighbors(method="cutoff", cutoff=rmax)
    distances = list(itertools.chain(*system.atoms.neighbors.distance))

    hist, bin_edges = np.histogram(distances, bins=bins, 
        range=(rmin, rmax))
    edgewidth = np.abs(bin_edges[1]-bin_edges[0])
    hist = hist.astype(float)
    r = bin_edges[:-1]

    #get box density
    rho = int(system.natoms*(system.natoms-1)*0.5)/system.volume
    shell_vols = (4./3.)*np.pi*((r+edgewidth)**3 - r**3)
    shell_rho = hist/shell_vols
    #now divide to get final value
    rdf = shell_rho/rho
    plt.plot(r, rdf)
    plt.xlabel(r'$r (\AA)$')
    plt.ylabel(r'$g(r)$')
    return r, rdf