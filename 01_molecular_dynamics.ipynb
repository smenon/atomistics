{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "16c6af3f-23fc-4493-8dc8-03c1dbc25bf5",
   "metadata": {},
   "source": [
    "# <font style=\"font-family:roboto;color:#455e6c\"> Atomistic Simulations for Thermodynamics </font>  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0f4ca7cd-8839-443e-b98c-a1a71225b57a",
   "metadata": {},
   "source": [
    "<div class=\"admonition note\" name=\"html-admonition\" style=\"background:#e3f2fd; padding: 10px\">\n",
    "<font style=\"font-family:roboto;color:#455e6c\"> Sarath Menon</font> </br>\n",
    "<font style=\"font-family:roboto;color:#455e6c\"> Max Planck Institute for Sustainable Materials </font> </br>\n",
    "<font style=\"font-family:roboto;color:#455e6c\"> <b> SusMet Lecture Series: Thermodynamics </b> </font> </br>\n",
    "<font style=\"font-family:roboto;color:#455e6c\"> 04 Sept 2024 </font>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ba0906dd-926e-4847-8376-1641977ed378",
   "metadata": {},
   "source": [
    "<table border=\"0\">\n",
    "<tr>\n",
    "  <td>\n",
    "    <font style=\"font-size:14px\">In the previous lecture, we explored the concepts of thermodynamics and statistical mechanics in detail, including ensembles and key quantities such as enthalpy, entropy, and free energy. We also discussed their practical applications and the valuable insights they provide about material systems. In this session, we will focus on how to calculate these quantities on an atomic scale using simulations. The main questions we will address are:\n",
    "\n",
    "- How can we calculate these thermodynamic quantities, starting at the atomic level?\n",
    "- How can we model a material using simulations?</font> </td>\n",
    "  <td>\n",
    "    <img src=\"images/cartoon.png\" width=\"100%\" align=\"right\"></td>\n",
    "</tr>\n",
    "<tr></tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25817a12-50c2-4618-aeb3-b8fcb1b03203",
   "metadata": {},
   "source": [
    "## <font style=\"font-family:roboto;color:#455e6c\"> Modelling of materials </font> "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a438ea40-9da5-4dcf-a9f4-518833323c5d",
   "metadata": {},
   "source": [
    "Materials can be modeled across various length and time scales. In this session, we will focus on the atomistic scale and explore how the concepts from the previous lecture apply at this level."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4f9a5a3e-5e99-4d4a-9028-ace62d0deb54",
   "metadata": {},
   "source": [
    "<img src=\"images/multiscale.png\" width=\"50%\">"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ed5ae13e-72f1-45e8-84ed-6f35fddc4ca1",
   "metadata": {},
   "source": [
    "<font style=\"font-size:8px\"> Image source: icams.de </font>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cced63e5-7e83-40fd-8133-f3dceac9d3a1",
   "metadata": {},
   "source": [
    "## <font style=\"font-family:roboto;color:#455e6c\"> Thermodynamic ensembles </font> "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0f74be4d-d36c-4615-ae03-01ac249cd85c",
   "metadata": {},
   "source": [
    "Understanding any process at the atomic scale involves interpreting the laws of thermodynamics at that level. We will begin by reviewing the thermodynamic ensembles discussed in the last lecture."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "03ba1b78-e0b0-4b64-9a32-768efc91533b",
   "metadata": {},
   "source": [
    "Imagine a single-component system. For such a system of particles (in this case, atoms), we can envision all the possible states it can occupy. Each atom has three position components ($x$, $y$, $z$—collectively denoted as $\\pmb{r}$) and three momentum components ($p_x$, $p_y$, $p_z$—collectively denoted as $\\pmb{p}$), giving each atom 6 degrees of freedom. For a system of $N$ atoms, this results in a $6N$-dimensional space, known as **phase space**.\n",
    "\n",
    "However, we know that the thermodynamic state of such a system can be described by a small number of macroscopic parameters, such as:\n",
    "\n",
    "- The number of particles, $N$\n",
    "- Temperature, $T$\n",
    "- Pressure, $P$\n",
    "- Volume, $V$\n",
    "\n",
    "Other thermodynamic properties, such as chemical potential, density, or specific heat, depend on these parameters and can be derived from thermodynamic equations, as discussed earlier. By constraining some of these macroscopic parameters, we only need to consider a subset of the phase space, leading to the idea of thermodynamic ensembles."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "321b3398-4ace-4920-903a-0ac835701c23",
   "metadata": {},
   "source": [
    "### <font style=\"font-family:roboto;color:#455e6c\"> Microcanonical ensemble </font> \n",
    "\n",
    "- System is at constant: number of particles ($N$), volume ($V$), and energy ($E$)\n",
    "- All states are indistinguishable and equally probable\n",
    "- The partition function is given by: $$Q_\\mathrm{NVE} = \\frac{1}{N!} \\frac{1}{h^{3N}} \\int d \\pmb{r} d \\pmb{p} \\delta(H(\\pmb{r}, \\pmb{p}) - E)$$\n",
    "\n",
    "Here, $\\pmb{r} d \\pmb{p}$ represents the entire phase space, $\\delta$ is the Kronecker delta, indicating that we are only interested in a subset of states in the phase space where the energy is exactly $E$.\n",
    "\n",
    "$H(\\pmb{r}, \\pmb{p})$ is the Hamiltonian of the system, which is the sum of the kinetic and potential energies, $H(\\pmb{r}, \\pmb{p}) = K(\\pmb{p}) + U(\\pmb{r})$. While we know how to calculate the kinetic energy, the potential energy is more complex—we will revisit this topic shortly.\n",
    "\n",
    "In this ensemble, the thermodynamic potential is the entropy $(S)$, given by:\n",
    "\n",
    "$$S = -k_\\mathrm{B} \\log(Q_\\mathrm{NVE})$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fac4b4ae-da79-4302-b2d0-7de3aa0b262a",
   "metadata": {},
   "source": [
    "### <font style=\"font-family:roboto;color:#455e6c\"> Canonical ensemble </font> \n",
    "\n",
    "- System is at constant: number of particles ($N$), volume ($V$), and temperature ($T$)\n",
    "- The partition function is given by: $$Q_\\mathrm{NVT} = \\frac{1}{N!} \\frac{1}{h^{3N}} \\int d \\pmb{r} d \\pmb{p} \\exp{-(H(\\pmb{r}, \\pmb{p})/k_\\mathrm{B}T)}$$\n",
    "\n",
    "In this ensemble, the thermodynamic potential is the Helmholtz free energy $(F)$, expressed as:\n",
    "\n",
    "$$F = -k_\\mathrm{B} T \\log(Q_\\mathrm{NVT})$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f1bc0b2b-8de2-48f1-a9dd-092a4939d166",
   "metadata": {},
   "source": [
    "### <font style=\"font-family:roboto;color:#455e6c\"> Isothermal-Isobaric ensemble </font> \n",
    "\n",
    "- System is at constant: number of particles ($N$), volume ($P$), and temperature ($T$)\n",
    "- partition function: $$Q_\\mathrm{NPT} = \\frac{1}{N!} \\frac{1}{h^{3N}} \\int d \\pmb{r} d \\pmb{p} \\exp{-((H(\\pmb{r}, \\pmb{p}) + PV)/k_\\mathrm{B}T)}$$\n",
    "\n",
    "The thermodynamic potential in this ensemble is Gibbs free energy $(G)$,\n",
    "\n",
    "$$G = -k_\\mathrm{B} T \\log(Q_\\mathrm{NPT})$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "67978c58-f47c-4b7e-897a-6f32688306f0",
   "metadata": {},
   "source": [
    "There are also others, such as the Grandcanonical ensemble."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "65b54c35-e154-43e9-97df-0d6e8a699f65",
   "metadata": {},
   "source": [
    "## <font style=\"font-family:roboto;color:#455e6c\"> Ensemble averages </font> "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ca17a05-c272-42e5-88d4-0111b95bfa22",
   "metadata": {},
   "source": [
    "So, what are ensembles? We know that phase space represents all possible states of a system. If we consider a phase space coordinate $\\pmb{x}$, an ensemble describes the probability distribution ($\\rho$) of these phase space points. In the microcanonical ensemble, all points are equally probable. In the canonical ensemble, the probability is proportional to $\\exp{\\left(-\\frac{H(\\pmb{r}, \\pmb{p})}{k_\\mathrm{B}T}\\right)}$.\n",
    "\n",
    "This means that to measure a quantity $A$ in a given ensemble, we must evaluate $A$ for each phase space coordinate $\\pmb{x}$ and then average over all these values:\n",
    "\n",
    "$$A_\\mathrm{observable} = \\langle A \\rangle_\\mathrm{ensemble} = \\sum_{\\pmb{x}_i} A(\\pmb{x}_i) \\rho(\\pmb{x}_i)$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "893e9aaa-c1e7-4a82-9096-7ef00de93450",
   "metadata": {},
   "source": [
    "We have learned about ensembles and how to measure quantities within them as ensemble averages. Now, let's explore how to model a system of $N$ particles."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "193f97ee-dbec-46b5-942b-b37cd855f134",
   "metadata": {},
   "source": [
    "## <font style=\"font-family:roboto;color:#455e6c\"> Molecular Dynamics </font> "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "efff1383-9bb2-4de9-a30d-fcd20ac7f879",
   "metadata": {},
   "source": [
    "Molecular dynamics (MD) models the time evolution of a system of $N$ particles in a classical framework. Since the system is classical, we can apply Newton's equations of motion to simulate the behavior of the particles. This is the fundamental idea behind molecular dynamics.\n",
    "\n",
    "A basic MD algorithm follows these steps:\n",
    "\n",
    "- Start with an initial configuration of $N$ atoms, each with a defined position and velocity.\n",
    "- Calculate the force on each atom using $\\pmb{f}_i = - \\nabla U(\\pmb{r})$.\n",
    "- Using the positions, velocities, and forces, update the positions over time with an appropriate integrator.\n",
    "- Recalculate the forces with the updated positions, and repeat the process.\n",
    "\n",
    "This straightforward set of instructions ensures that the system evolves over time. An important aspect to consider is the choice of integrator. There are various integrators available, with one of the most commonly used being the Velocity Verlet algorithm. Below are the relevant equations, where $t$ is the time at which we know the positions, velocities, and forces, and $t + \\Delta t$ is the target time. The index $i$ is omitted for simplicity.\n",
    "\n",
    "$$\n",
    "\\pmb{r}(t + \\Delta t) = \\pmb{r}(t) + \\pmb{v}(t) \\Delta t +\\frac{1}{2} \\pmb{a}(t) \\Delta t^2 \n",
    "$$\n",
    "\n",
    "$$\n",
    "\\pmb{a}(t + \\Delta t) = f(\\pmb{r}(t + \\Delta t))\n",
    "$$\n",
    "\n",
    "$$\n",
    "\\pmb{v}(t + \\Delta t) = \\pmb{v}(t) + \\frac{1}{2} (\\pmb{a}(t) + \\pmb{a}(t + \\Delta t)) \\Delta t \n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bb392433-4b13-4a67-b552-dc9afccc54a3",
   "metadata": {},
   "source": [
    "The final piece we're missing, as we briefly mentioned earlier, is the exact nature of $U(\\pmb{r})$. We'll explore this in detail shortly."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1bd65fc2-579d-460f-a3f9-1e64246903a5",
   "metadata": {},
   "source": [
    "With this simple iteration scheme, we can explore how the system evolves over time. For a measurable property, the time average is given by:\n",
    "\n",
    "$$\n",
    "\\langle A \\rangle_{\\mathrm{time}} = \\lim_{t^\\prime \\to \\infty} {\\frac{1}{t^\\prime} \\int_0^{t^\\prime}} A(\\pmb{x}(t)) dt\n",
    "$$\n",
    "\n",
    "where $\\pmb{x}$ represents a phase space configuration.\n",
    "\n",
    "However, our primary interest lies in ensemble averages. So, how does the time average relate to ensemble averages?\n",
    "\n",
    "This is where the **ergodic hypothesis** becomes crucial. The ergodic hypothesis suggests that, for a sufficiently long time interval, the ensemble average is equivalent to the time average. This allows us to evaluate the macroscopic behavior of the system through its time evolution in a thermodynamic ensemble. If a macroscopic property can be expressed as an ensemble average, it can be computed from MD simulations!\n",
    "\n",
    "Finally, note that the equations above pertain to the NVE ensemble, which is the default for MD simulations. To control temperature and pressure, we need to incorporate thermostats and barostats."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44df7ca2-42ca-4b28-b817-0f8506054d3f",
   "metadata": {},
   "source": [
    "### <font style=\"font-family:roboto;color:#455e6c\"> Interatomic potentials </font> "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce5de7a1-c48f-4ba7-8c53-c33c916faad9",
   "metadata": {},
   "source": [
    "The final piece to complete the picture is $U(\\pmb{r})$, which models the interaction between atoms and provides the basis for calculating forces and energy. This function, known as the interatomic potential, depends on the positions of the atoms. The accuracy of the quantities we observe in simulations relies heavily on this potential. We will discuss it in more detail later today."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4762aa9a-e991-4682-bc01-d9e5ef1c878d",
   "metadata": {},
   "source": [
    "## <font style=\"font-family:roboto;color:#455e6c\"> Hands-on simulations </font> "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4aa5dfec-619a-47a2-b1cb-47f991501e41",
   "metadata": {},
   "source": [
    "We are finally ready to run some simulations. We are working in what is known as a Jupyter notebook, which is an interactive editor for Python (and other languages). It allows to write text and code in python together. Some basic commands which would be helpful:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1e094d4c-f13d-48bb-ab1c-9480e018727e",
   "metadata": {},
   "source": [
    "* Select cells by clicking on them\n",
    "* Navigate through with `up` and `down` keys\n",
    "* `Enter` on a cell to edit it\n",
    "* `Esc` to stop editing\n",
    "* `Alt+Enter` to execute cell\n",
    "* Copy, cut and paste them with `C`, `X` and `V`\n",
    "* `A` to insert cell above\n",
    "* `B` to insert cell below\n",
    "* `M` to convert a cell to Markdown, wherein you can write text\n",
    "\n",
    "You do not have to remember these commands, icons are available on the toolbar above."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "193d9631-d9ff-4756-bd42-a72312df59a4",
   "metadata": {},
   "source": [
    "We will use `pyiron`, a workflow tool developed here at MPI SusMat. `pyiron` makes running these simulations quite easy, without having to learn the details of a simulation code."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a53096f7-3f27-4bc7-a8cf-07f1b49ac691",
   "metadata": {},
   "source": [
    "The central object is `pyiron` is called the `Project`. You can think of project exactly as your `Project` in conventional sense. It allows to name the project as well as to derive all other objects such as structures, jobs etc. without having to import them. Thus, by code completion *Tab* the respective commands can be found easily.\n",
    "\n",
    "We now create a pyiron Project named 'first_steps'."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1e6b4e77-e20d-4289-8d69-400a5fa1dc49",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyiron import Project"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "64dda671-38cd-48e4-8758-53c36a56d691",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f80fe07e-a10b-4dfe-82de-383f82a51a35",
   "metadata": {},
   "source": [
    "We now create a first project"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8cb889b7-0dc4-4c49-bf81-b0cc268ef28c",
   "metadata": {},
   "outputs": [],
   "source": [
    "pr = Project('md')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16613ed2-db69-490b-bdb3-2309bdad481b",
   "metadata": {},
   "source": [
    "The project name also applies for the directory that is created for the project. All data generated by this `Project` object resides in this directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2c48e2d7-6eef-4119-ae8e-e8187a1f8e59",
   "metadata": {},
   "outputs": [],
   "source": [
    "pr.path"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "edcd52dd-9452-4c05-b1a9-b30960a13b63",
   "metadata": {},
   "source": [
    "### <font style=\"font-family:roboto;color:#455e6c\"> Creating atomic structures </font> "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7ce0544f-b9b2-46b2-9884-c25d0fc206e4",
   "metadata": {},
   "source": [
    "As we saw, the first step is to have initial positions (and velocities, which we will see soon). Since we are dealing with atomic systems, that is the atomic structure. or more details on generating and manipulating structures, please have a look at our [structures example](https://pyiron.readthedocs.io/en/latest/source/notebooks/structures.html). In this section however, we show how to generate and manipulate bulk crystals, surfaces, etc. pyiron's structure class is derived from the popular [`ase` package](https://wiki.fysik.dtu.dk/ase/ase/build/build.html) and any `ase` function to manipulate structures can also be applied here."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "98d6036c-4cd0-4b52-98f4-d009995b3a21",
   "metadata": {},
   "source": [
    "We will choose Al as an example material. We create an Al structure"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "38c67cb6-d470-4149-8f08-6c5f0f1b6a58",
   "metadata": {},
   "outputs": [],
   "source": [
    "struct = pr.create.structure.bulk('Al', cubic=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "91f03d60-2992-405d-8d25-ea87aa6ba71c",
   "metadata": {},
   "outputs": [],
   "source": [
    "struct.plot3d()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "69b41772-2449-4945-b747-56f705adbc82",
   "metadata": {},
   "source": [
    "As we can see, it is an Al FCC cell with four atoms. Now we take a look at how we run an MD simulation with this structure. Once we have an atomic structure, we can set up a simulation \"job\" of any atomistic simulation that is intergrated within pyiron. In this section, we are going to use the popular [LAMMPS code](https://lammps.sandia.gov/)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e909e430-1228-460a-834d-010ad5384520",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a job\n",
    "job = pr.create.job.Lammps(job_name=\"md100\", delete_existing_job=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5bbd6801-23e2-40b2-aee7-55e5689b5d42",
   "metadata": {},
   "source": [
    "Now, as we discussed before, we need a set of initial positions, in the form of a structure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0c22aeb0-ac8f-437b-ad3b-359e5231cf4a",
   "metadata": {},
   "outputs": [],
   "source": [
    "job.structure = struct.repeat(4)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "36fe4acd-d7dc-45d7-8f6a-1af6f627c6ee",
   "metadata": {},
   "source": [
    "Now we need a model for the atomic interactions, or $U(\\pmb{r})$ in our equations above. There are many different models available, which we can list easily:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ea35eb43-20eb-4f5f-b70a-24c9914b0498",
   "metadata": {},
   "outputs": [],
   "source": [
    "# See available potentials\n",
    "job.list_potentials()[20:30]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2806a58e-41c5-4ffd-92eb-9e180b177675",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Choose one of these potentials\n",
    "job.potential = \"2005--Mendelev-M-I--Al-Fe--LAMMPS--ipr1\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9e5dc065-546e-43e1-b5d1-10ff58711bda",
   "metadata": {},
   "source": [
    "The upcoming lectures will go into more details on the specific of these potentials, and the choice of the model for the task at hand. For now, we will choose a model."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42e99784-9afd-4224-a908-01d22996afed",
   "metadata": {},
   "source": [
    "At this stage, the computational parameters for the simulation needs to be specified. pyiron parses generic computational parameters into code specific parameters allowing for an easy transition between simulation codes"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "472c4d8b-ff13-4c25-8300-95318fea05c1",
   "metadata": {},
   "source": [
    "You we need to choose the ensemble we will work in, we will choose NPT, or the Isothermal-Isobaric ensemble. For this, we need to constrain the number of atoms, temperature, and pressure. This is done with the help of the `calc_md` command. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ed11d2df-0899-4acb-8c78-107492bc4a92",
   "metadata": {},
   "outputs": [],
   "source": [
    "# specify calculation details: in this case: MD at 100 K in the NPT ensemble (pressure=0) for 10000 steps\n",
    "job.calc_md(temperature=100, pressure=0, n_ionic_steps=10000)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4710bf1d-c297-43ac-b64f-5176f2787c1c",
   "metadata": {},
   "source": [
    "`n_ionic_steps` is the number of times the equation of motion should be integrated, to propogate the system forward in time."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6867fd0d-c78e-49a6-89bb-a45e1069f0e8",
   "metadata": {},
   "source": [
    "Once the `run()` commmand is called, pyiron creates necessary input files, calls the simulation code, and finally parses and stores the output."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "055582ce-8565-4a03-be10-063be91a54cd",
   "metadata": {},
   "outputs": [],
   "source": [
    "job.run()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "96b5b119-e367-4b1a-b9d5-b55a78c30bdc",
   "metadata": {},
   "source": [
    "Now, we can take a look at the output."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0df2029a-2dfd-4415-b9e7-ff66a1f4e1f0",
   "metadata": {},
   "outputs": [],
   "source": [
    "job"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4f001c8d-9750-4b00-b4ee-f56aeaf194b0",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "job['output']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "578f88f6-d256-4eda-bbbe-248139332f41",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(job.output.energy_pot)\n",
    "plt.xlabel('Timestep')\n",
    "plt.ylabel('U (eV)')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9c3d8cce-f90d-427a-a5e1-f789b4de9854",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(job.output.temperature)\n",
    "plt.xlabel('Timestep')\n",
    "plt.ylabel('Temperature (K)')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "117c5f15-0ea0-4971-a0ea-13c21e6f12dc",
   "metadata": {},
   "source": [
    "Here, we can also see what we discussed about the time average, and instantaneous quantities. Although we set the temperature to be 100 K, we see that it fluctuates. What we are really interested is the ensemble average (which is also the time average through ergodic hypothesis)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "77526bca-09f8-4318-a51e-c4b78acb1a1b",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(job.output.temperature)\n",
    "plt.axhline(np.mean(job.output.temperature), c='black', ls='dashed')\n",
    "plt.xlabel('Timestep')\n",
    "plt.ylabel('Temperature (K)')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e5fd070d-bef8-43d4-b9b9-d59ebf18e8cf",
   "metadata": {},
   "source": [
    "We also take a look at how the atoms move "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5e82fee4-a552-4ada-9ff4-98c66fff25ed",
   "metadata": {},
   "outputs": [],
   "source": [
    "job.animate_structures()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8cb91c79-7c76-495a-8926-dbae87671d7d",
   "metadata": {},
   "source": [
    "<table border=\"0\">\n",
    "<tr>\n",
    "  <td>\n",
    "    <font style=\"font-size:14px\">\n",
    "        Another quantity we can look at is the radial distribution function (or pair correlation function). It measures how the density of atoms in a system varies as a function of the distance from a given reference particle. It gives information about where the atoms are in the system, and is quantity that can also be measured experimentally. It can be estimated by calculating distances between all pairs of atoms, and mnaking a histogram from the resulting data.\n",
    "    </font> </td>\n",
    "  <td>\n",
    "    <img src=\"images/rdf.png\" width=\"50%\" align=\"right\"></td>\n",
    "</tr>\n",
    "<tr></tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ca59b3b9-f44d-4e0e-a03f-b0b21eecdd97",
   "metadata": {},
   "outputs": [],
   "source": [
    "from rdf import calculate_rdf"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6dfbf24e-fcb7-406a-bef3-9fb6e8453635",
   "metadata": {},
   "outputs": [],
   "source": [
    "r100, rdf100 = calculate_rdf(job)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e0098398-9c53-44be-a615-4678d0b60c0f",
   "metadata": {},
   "source": [
    "At the temperature of 100 K, we can see that there are still sharp peaks, which means the atoms are vibrating, but still rather around their initial positions. Let us increase the temperature (and use a bigger system), and see what happens."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2ecdfd7a-b046-4537-af6b-c0da69e28210",
   "metadata": {},
   "outputs": [],
   "source": [
    "job = pr.create.job.Lammps(job_name=\"md800\", delete_existing_job=True)\n",
    "job.structure = pr.create.structure.bulk('Al', cubic=True).repeat(4)\n",
    "job.potential = \"2005--Mendelev-M-I--Al-Fe--LAMMPS--ipr1\"\n",
    "job.calc_md(temperature=800, pressure=0, n_ionic_steps=10000)\n",
    "job.run()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b2431309-f43b-4f40-8c17-adae5980e231",
   "metadata": {},
   "outputs": [],
   "source": [
    "r800, rdf800 = calculate_rdf(job)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2693b3a7-3b89-48c4-9c78-71133476b498",
   "metadata": {},
   "source": [
    "We can see that the RDF has changed dramatically! Only the first peak is sharp due to the strong vibrations of the atoms. Now we check at an even higher temperature."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7c8fdd1e-ff93-47ce-884e-4ece94e69b9e",
   "metadata": {},
   "outputs": [],
   "source": [
    "job = pr.create.job.Lammps(job_name=\"md1500\", delete_existing_job=True)\n",
    "job.structure = pr.create.structure.bulk('Al', cubic=True).repeat(4)\n",
    "job.potential = \"2005--Mendelev-M-I--Al-Fe--LAMMPS--ipr1\"\n",
    "job.calc_md(temperature=1500, pressure=0, n_ionic_steps=10000)\n",
    "job.run()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "76b7baeb-5890-4993-b370-90e400691803",
   "metadata": {},
   "outputs": [],
   "source": [
    "r1500, rdf1500 = calculate_rdf(job)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "58d69206-4250-4394-a090-18158ee39e6f",
   "metadata": {},
   "source": [
    "Plotting them together:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "00ecb435-f57c-4e4d-b17a-6a40e65c2cf6",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(r100, rdf100, label='100 K')\n",
    "plt.plot(r800, rdf800, label='800 K')\n",
    "plt.plot(r1500, rdf1500, label='1500 K')\n",
    "plt.xlabel(r'$r (\\AA)$')\n",
    "plt.ylabel(r'$g(r)$')\n",
    "plt.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "137b571e-5e5a-42d1-8ba5-36f677fddfe6",
   "metadata": {},
   "source": [
    "What happened in this case?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f0b6761-de73-4a2b-a324-75895c9c7948",
   "metadata": {},
   "source": [
    "In this notebook we learned about ensembles, thermodynamics on atomic scale, and about running MD calculations. In the next notebook, we will calculate Helmholtz and Gibbs free energy."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fd7c5675-55a6-44ee-8508-a4a9fa665823",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true
   },
   "source": [
    "### <font style=\"color:#455e6c\" face=\"Helvetica\" > Further reading </font>\n",
    "- Allen, M. P. & Tildesley, D. J. Computer Simulation of Liquids. (Oxford university press, Oxford, 2017).\n",
    "- Smit, B. & Frenkel, D. Understanding Molecular Simulation, Second Edition (Computational Science Series, Vol 1)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bce9d0ac-2477-4da7-8b88-b151e8080a26",
   "metadata": {},
   "source": [
    "### <font style=\"font-family:roboto;color:#455e6c\"> Software used in this notebook </font>  \n",
    "\n",
    "- [pyiron_atomistics](https://github.com/pyiron/pyiron_atomistics)\n",
    "- [LAMMPS](https://www.lammps.org/)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
